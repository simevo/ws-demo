import WebSocket, { WebSocketServer } from "ws"

console.log("Server avviato")
const wss = new WebSocketServer({ port: 8000 })

wss.on("connection", function connection(ws, req) {
  // console.log(`headers: ${JSON.stringify(req.headers)}`)
  const ip = req.headers["x-forwarded-for"].split(",")[0].trim()
  // console.log("Nuova connessione da: " + ip)

  ws.on("error", console.error);

  ws.on("message", function message(data) {
    // console.log(`messaggio ricevuto: ${data}`)
    if (data = "sveglia tutti!") {
      wss.clients.forEach(function each(client) {
        // console.log(`client = ${JSON.stringify(client)}`)
        if (client.readyState === WebSocket.OPEN) {
          // console.log(`invio nuovi dati meteo a ${ip}...`)
          client.send("Sveglia, ci sono dei dati aggiornati!")
        }
      })
    }
  })
})
