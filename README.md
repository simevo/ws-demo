ws-demo
=======

Demo di canale di comunicazione bidirezionale asincrono di lunga durata tra molti client e un webserver implementato con websockets.

# Provalo!

1. Scarica il repo ed entra nella dir `ws-demo`:

        git clone https://gitlab.com/simevo/ws-demo
        cd ws-demo

2. In un terminale lancia il docker compose:

        docker-compose up

3. Apri almeno due finestre browser affiancate alla pagina http://localhost:8080/

4. Clicca il tasto "_Sveglia tutti!_" in una delle finestre e nota come il messaggio "_Sveglia, ci sono dei dati aggiornati!_" appare in tutti i logs.

Mandatory screencast:

![screencast](/screencast.gif)

# Riferimenti

- https://github.com/websockets/ws#server-broadcast

- https://www.nginx.com/blog/websocket-nginx/

- https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_client_applications

- https://github.com/nodejs/docker-node/blob/main/README.md#how-to-use-this-image


